extends Control

onready var language_option: OptionButton = $CenterContainer/VBoxContainer/LanguageOption

func _ready() -> void:
	var current_locale = OS.get_locale_language()
	var idx := 0
	for language in TranslationServer.get_loaded_locales():
		var language_name := TranslationServer.get_locale_name(language)
		language_option.add_item(language_name)
		language_option.set_item_metadata(idx, language)
		print([current_locale, language_name])
		if current_locale == language:
			language_option.select(idx)
		idx += 1

func _on_LanguageOption_item_selected(index: int) -> void:
	var language_name = language_option.get_item_metadata(language_option.selected)
	TranslationServer.set_locale(language_name)
